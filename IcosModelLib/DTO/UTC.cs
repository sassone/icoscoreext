﻿using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcosModelLib.DTO
{
    public class UTC : BaseClass
    {
        public UTC()
        {
            GroupId = (int)Globals.Groups.GRP_UTC_OFFSET;
        }
        public decimal UTC_OFFSET { get; set; }

        public string UTC_OFFSET_DATE_START { get; set; }
        public string UTC_OFFSET_COMMENT { get; set; }
    }
}
