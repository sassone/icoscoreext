﻿using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcosModelLib.DTO
{
    public class CLIM_AVG : BaseClass
    {
        public CLIM_AVG()
        {
            GroupId = (int)Globals.Groups.GRP_CLIM_AVG;
        }
        public decimal MAT { get; set; }

        public decimal MAP { get; set; }

        public decimal MAR { get; set; }

        public decimal MAC_YEARS { get; set; }

        public decimal MAS { get; set; }

        public string CLIMATE_KOEPPEN { get; set; }

        public string MAC_COMMENTS { get; set; }

        public string MAC_DATE { get; set; }
    }
}
