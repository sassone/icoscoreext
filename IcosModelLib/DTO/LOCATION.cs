﻿using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcosModelLib.DTO
{
    public class LOCATION : BaseClass
    {
        public LOCATION()
        {
            GroupId = (int)Globals.Groups.GRP_LOCATION;
        }

        public decimal LOCATION_LAT { get; set; }

        public decimal LOCATION_LONG { get; set; }

        public decimal LOCATION_ELEV { get; set; }

        public string LOCATION_DATE { get; set; }

        public string LOCATION_COMMENT { get; set; }
    }
}
