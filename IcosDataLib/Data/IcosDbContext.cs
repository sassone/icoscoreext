﻿//using Microsoft.Analytics.Interfaces;
//using Microsoft.Analytics.Types.Sql;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using IcosModelLib.DTO;

namespace IcosDataLib.Data
{
    public class IcosDbContext : DbContext
    {
        public IcosDbContext(DbContextOptions<IcosDbContext> options) : base(options)
        {

        }

        public DbSet<LOCATION> LOCATION { get; set; }

        public DbSet<UTC> UTC { get; set; }

        public DbSet<CLIM_AVG> CLIM_AVG { get; set; }

       // public DbSet<Site> Sites { get; set; }

        public DbSet<TEAM> TEAM { get; set; }

    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<IcosDbContext>
    {
        public IcosDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../IcosWebApi/appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<IcosDbContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);
            return new IcosDbContext(builder.Options);
        }
    }
}